import requests
import json

SITE_URL = "https://sprawlmap.org"
SLACK_LINK = "https://hooks.slack.com/services/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"

def main():
    response = requests.get(SITE_URL)
    status = response.status_code
    if status == 200:
        send_slack_message("sprawlmap.org is working")
    else:
        send_slack_message("sprawlmap.org is not working")

    
def send_slack_message(message):
    requests.post(SLACK_LINK, json.dumps({'text': message}))


if __name__ == "__main__":
    main()