
import pandas as pd
LOGFILENAME = "./apache/Apache_2k.log"

def read_as_df():
    df = pd.read_csv(LOGFILENAME,
              sep=r'\s',
              engine='python',
              usecols=[0, 1, 2,],
              names=['time', 'status', 'method'],
              na_values='-',
              header=None
    )

    return(df)


print(read_as_df().tail())