from flask import Flask, jsonify
from flask_restplus import Resource, Api
import pandas as pd
import json
from pandas_datareader import data
app = Flask(__name__)
api = Api(app)
from flask import make_response


        
@api.route('/<string:ticker>/')
class stock_price(Resource):
    def get(self, ticker):
        return data.DataReader(ticker, data_source='yahoo')['Adj Close'][-1]


@api.route('/<string:ticker>/<string:price_type>')
class stock_price(Resource):
    def get(self, ticker, price_type):
        return data.DataReader(ticker, data_source='yahoo')[price_type][-1]


@api.route('/<string:ticker>/<string:price_type>/<string:start_date>/<string:end_date>')
class stock_price(Resource):
    def get(self, ticker, price_type, start_date, end_date):
        return data.DataReader(ticker,
                        start=start_date,
                        end=end_date,  
                       data_source='yahoo')[price_type]

if __name__ == '__main__':
    app.run(debug=True)
